package com.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 * Created by WangKM on 2017/3/4.
 */
@RestController
@RequestMapping("test")
public class TestController {

    @ResponseBody
    @RequestMapping(value = "get", method = RequestMethod.GET)
    public Object get(HttpServletRequest request) throws JsonProcessingException {
        Logger logger = LoggerFactory.getLogger(TestController.class);
        ObjectMapper mapper = new ObjectMapper();
        logger.info(mapper.writeValueAsString(request.getParameterMap()));
        return request.getParameterMap();
    }


    @ResponseBody
    @RequestMapping(value = "post", method = RequestMethod.POST)
    public Object post(@RequestBody Object request) {
        return request;
    }

}
